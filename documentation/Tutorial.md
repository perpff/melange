# Step by step guide to run an example script

Here, we download and run one of the scripts provided by the 'various-example-scripts' package (separate download) or simply by the documented simulation-file included in the documentation.

## I) Install 'Melange'
We assume here that you already installed 'Melange', i.e. that you have a working 'melange.so' file somewhere on your system. If not, please read the 'INSTALL' file, which you can either download separately (as html), or you can just use the one coming with this documentation archive (as pdf).
 
## II) Download a working simulation file
An example simulation script is included in the documentation archive. You can also use one of the simulations provided by the 'example simulations' archive.

## III) Preparations
We recommend to create a separate directory for every simulation. Also, you should have at least a faint idea of how to work with a linux terminal (see [here](https://help.ubuntu.com/community/UsingTheTerminal)).

If you didn't install 'Melange' globally, you have to copy the 'melange.so' library into that directory. Furher, you need to copy the simulation script there. Again, see the INSTALL file.

You can perform these file operations with the standard file manager. 

If you want to use a terminal to perform these steps, do the following. This might be a good excercise if you are completely unfamilar with terminals. If we assume that you want to have the simulation directory just below your home directory ('/home/you' on Linux systems):

* open terminal
* go straight to your home directory if you are not there: 
	* type 'cd' (followed by 'enter').
* create the new simulation directory: 
	* type 'mkdir simulation_directory'
	* you may substitue 'simulation_directory' by any name you like for the directory
* go into the new directory:
	* 'cd simulation_directory'
* copy the files that you need to the directory:
	* 'cp /path/to/simulation-file.py .'
	* 'cp /path/to/melange.os .'     
*Note:* you don't need the last command if you installed the file 'melange.os' in a path were the Python interpreter can find it. See 'INSTALL' file for details, or install from the deb file.

## Run
* open a terminal or just stay in the open terminal
	* if you opened a new one, navigate to the directoy:
		* 'cd'
		* 'cd simulation_directory'
* run the script:
	* 'python simulation-file.py'
* wait till the simulation finished, or at least till the first .vtu files appear in the directory. 

Simulations may take a long time! If you want to stop the execution, go to the terminal where you are running the simulation and hit 'Ctrl-C'. This will stop the running simualation once the current timestep is over.

## View the results
'Melange' stores the results as vtu-files, which can be viewed with applications using the vtk-toolkit. We recommend to use 'Paraview' for this purpose. If you don't find it trivial to use Paraview (I didn't!) have a look at the 'Paraview' section in the 'Using Melange' file included with this documentaion.
