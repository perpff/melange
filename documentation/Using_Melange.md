

# Using 'Melange'

Below we describe what you need to know about the software, which software tools you will need (and how to use them) and how to run an eperiment. If you need or want a step-by-step walkthrough for an example run of 'Melange', you will find it in a different file.

# Introduction

## General remarks

'Melange' is basically a C++ library with a Python API. Experiments with 'Melange' require therefore a Python script as wrapper. But don't worry: the amount of Python you need to learn is very small. If you simply tweak the experiment files that we provided you probably *don't need to know anything about Python at all*.

However, if you want to make use of the full power of Python, you have it at your finger tips. And Python is -- despite being a very mighty programming language -- also a very userfriendly and quick to lear language. If you have experience with Matlab or any other programming language you can master the basics in an afternoon. A good Python tutorial can be found [here](http://docs.python.org/2/tutorial/).

## Supported platforms

We compiled 'Melange' for the Ubuntu Linux Distribution and in particular for the current version Ubuntu 12.10. You can download it [here](http://www.ubuntu.com/download/desktop) if you want to install it -- it's free of charge and can be installed along with Windows. Maybe you want to have a look at [distrowatch](http://distrowatch.com/table.php?distribution=ubuntu) for further information.

The instructions below were written with Ubuntu in mind. If this doesn't fit your needs, you can *very probably* (we didn't test this) recompile Melange on other Platforms as well, if you install the necessary dependencies. However, all the software tools and dependencies Melange uses are open source and exist on Linux, Windows and Mac.

## Multi processor support

'Melange' runs was written to support SMP machines, i.e. multi processor or multi core machines with a single shared memore. This setup is standard for most of todays computers. 

The performance depends largely on the specifics of your particular hardware and may be quite different from our experiences. However, in our tests the setups with the fastest computation times used 8 to 16 independent processors or cores. Using more than 16 processors seems to be slower than this again.

# Documented example scripts

The specific experimental setup is mainly documented inside the 'documented_simulation.py' script, which explains the setup of an experiment in some detail and contains most of the available relevant functions. It is also a good starting point for your own experiments, as all you have to do is to comment/uncomment some functions and to change parameters. Also the script needs to have a certain structure, which is guaranteed if you use an already working script as a basis.

We did also provide some less documented example simulations which we used in our own work. They might be a good starting point.

# Useful or indispensible tools

The single most helpful tool in Linux is called a terminal or a command line. It is very helpful to know what exactly a terminal is and how to open it. See [here](https://help.ubuntu.com/community/UsingTheTerminal). However, except for actually *running* the script, we can do with graphical tools as well. Below we mention both: graphical tools and terminal commands to achieve what we need to do.

In order to run a simuation we need to be able to do the following:

* edit the script with the experimental setup
	* GUI tools:
		* Gedit (a very basic text editor, but does the job)
		* Geany (a more advance text editor specialized on coding)
	* Terminal:
		* type: 'nano ' followed by the name of the file you want to edit. Don't type the quotes!
* run the experiment
	* Terminal -- this can be done *only* in the terminal
		* type: 'python simulation.py' if the simulation script is called 'simulation.py'. Replace the filename if necessary. Don't type the quotes!
* view the results
	* visualize in Mayavi or in Paraview. See below for instructions!

## Visualizing the results

'Melange' uses the .vtu file format to output the results. vtu-files can be read and displayed by applications based on the vtk toolkit. These applications are usually freely available. Examples are [Paraview](www.paraview.org) or [Mayavi](http://code.enthought.com/projects/mayavi). We do specifically recommend Paraview, because we are going to describe it in more detail below.

If you installed 'Melange' with the provided deb file in Ubuntu or Debian, Paraview should have installed as well in the process.

### Getting started with Paraview

If you have never used Paraview before, follow the steps below for a simple visualization. You can tweak the visual output in a **lot** of ways, which would be to much for this simple help file.

1. open one of the .vtu files
2. click on *Apply* (marked **1** in the screenshot below), which will load the data from the file
3. click on the 'glyph' button (**2**) (also in the 'Filter' menu)
4. settings in the *Properties* tab (**3**) should be *exactly* as shown in the screenshot
5. Hit *Apply*. This should give a first picture.
6. Center by clicking (**4**) in the toolbar.
7. Rotate by clicking into the window, pushing the left mouse button and moving the mouse around. Zoom with the mousewheel.

![Paraview widgets referenced in the text](Paraview.png)

Also have a close look into the settings provided by the *Display* tab!

Probably the most useful of the filters offered by Paraview is the 'Threshold' filter. Click it after you made your settings in the 'Glyph' filter, and you can select the data you want to be displayed. 

## Run an example simulation

Have a look into the Tutorial (included in this documentation) to get step-by-step instructions about how to run one of the example simulation files.