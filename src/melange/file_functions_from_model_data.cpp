/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

/*********************************

    as the name suggests,
    this file gather methods
    from all classes related
    to file-opening / file-
    saving functionality.

*********************************/

#include "model.h"

void Model::DumpVTKFile(string stdpath)
{
    // 1st open file for output, but repair the filename
    
    // erase trailing whitespaces:
	string whitespaces (" \t\f\v\n\r");
	size_t found;  
	found=stdpath.find_last_not_of(whitespaces);
	if (found!=string::npos)
		stdpath.erase(found+1);
	else
	{
		stdpath.clear();            // str is all whitespace
		cout << "no name for the outputfiles given!" << endl;
		exit (0);
	}
	
	// append ".vtu" if not already present
    found = stdpath.find_last_of(".");
    if ( found != string::npos )
    {
		if ( stdpath.substr(found) != ".vtu" )
			stdpath.append(".vtu");
	}
	else
		stdpath.append(".vtu");

	ofstream store;
	store.open(stdpath.c_str());
	

    // 2nd write header
    store << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\">" << endl;
    store << "<UnstructuredGrid>" << endl;
    store << "<Piece NumberOfPoints=\"" << nb_of_particles << "\" NumberOfCells=\"0\">" << endl;

    // 3rd write point positions
    VTK_SavePoints(&store);

    // 4th write point data
    store << "<PointData Scalars=\"radius\">" << endl;
    VTK_SavePointData_Radius(&store);
    VTK_SaveBoundaries(&store);
    VTK_SaveFractures(&store);
    VTK_SavePointData_ParticleNumber(&store);
    
    VTK_SavePointData_MeanStress(&store);
    VTK_SavePointData_DiffStress(&store);
    VTK_SavePointData_XStress(&store);
    VTK_SavePointData_YStress(&store);
    VTK_SavePointData_ZStress(&store);
    VTK_SavePointData_XZStress(&store);
    VTK_SavePointData_XYStress(&store);
    VTK_SavePointData_YZStress(&store);
    
	VTK_SavePointData_Sigma1(&store);
	VTK_SavePointData_Sigma2(&store);
	VTK_SavePointData_Sigma3(&store);
    VTK_SaveVecData_Sigma1_Vec(&store);
    VTK_SaveVecData_Sigma2_Vec(&store);
    VTK_SaveVecData_Sigma3_Vec(&store);
    
    VTK_SavePointData_YoungsModulus(&store);
    
	if (viscoelastic)
	{
		VTK_SavePointData_Viscosity(&store);
	}
	
	VTK_SavePointData_Density(&store);
    
//    VTK_SavePointData_Fix_Z(&store);
//    VTK_SavePointData_Fix_Y(&store);
//    VTK_SavePointData_Fix_X(&store);
    VTK_SavePointData_X_Layer(&store);
    VTK_SavePointData_Y_Layer(&store);
    VTK_SavePointData_Z_Layer(&store);
    VTK_SavePointData_X_Pos(&store);
    VTK_SavePointData_Y_Pos(&store);
    VTK_SavePointData_Z_Pos(&store);
    store << "</PointData>" << endl;

    // 5th write tail
    store << "<Cells>" << endl;
    store << "<DataArray type=\"Int32\" NumberOfComponents=\"0\" Name=\"connectivity\" format=\"ascii\">" << endl;
    store << "</DataArray>" << endl;
    store << "<DataArray type=\"Int32\" NumberOfComponents=\"0\" Name=\"offsets\" format=\"ascii\">" << endl;
    store << "</DataArray>" << endl;
    store << "<DataArray type=\"UInt8\" NumberOfComponents=\"0\" Name=\"types\" format=\"ascii\">" << endl;
    store << "</DataArray>" << endl;
    store << "</Cells>" << endl;
    store << "</Piece>" << endl;
    store << "</UnstructuredGrid>" << endl;
    store << "</VTKFile>" << endl;
	
    // 6th close file
    store.close();
}

void Model::VTK_SavePointData_Viscosity(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"viscosity\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].viscosity << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Density(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"density\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].density << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePoints(ofstream *store)
{
    *store << "<Points>" << endl;
    *store << "<DataArray NumberOfComponents=\"3\" type=\"Float64\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].pos[0] << " " << particle_list[i].pos[1] << " " << particle_list[i].pos[2] << " " << endl;
    }

    *store << "</DataArray>" << endl;
    *store << "</Points>" << endl;
}

void Model::VTK_SavePointData_MeanStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"mean_stress\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].mean_stress << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_DiffStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"diff_stress\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].diff_stress << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_XStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sxx\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sxx << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_YStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"syy\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].syy << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_ZStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"szz\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].szz << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_XYStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sxy\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sxy << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_XZStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sxz\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sxz << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_YZStress(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"syz\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].syz << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Sigma1(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma1\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma1 << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Sigma2(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma2\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma2 << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Sigma3(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma3\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma3 << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SaveVecData_Sigma1_Vec(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma1_vec\" NumberOfComponents=\"3\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma1_vec[0] << " " << particle_list[i].sigma1_vec[1] << " " << particle_list[i].sigma1_vec[2] << " " << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SaveVecData_Sigma2_Vec(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma2_vec\" NumberOfComponents=\"3\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma2_vec[0] << " " <<particle_list[i].sigma2_vec[1] << " " <<particle_list[i].sigma2_vec[2] << " " << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SaveVecData_Sigma3_Vec(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"sigma3_vec\" NumberOfComponents=\"3\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].sigma3_vec[0] << " " <<particle_list[i].sigma3_vec[1] << " " <<particle_list[i].sigma3_vec[2] << " " << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Radius(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"radius\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].radius << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SaveBoundaries(ofstream *store)
{
    *store << "<DataArray type=\"Int64\" Name=\"boundary\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        if (particle_list[i].is_boundary)
            *store << 1 << endl;
        else
            *store << 0 << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SaveFractures(ofstream *store)
{
	*store << "<DataArray type=\"Int64\" Name=\"2nd_neig_fracture\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;
	for (int i = 0; i < nb_of_particles; i++)
	{
		if (particle_list[i].second_neighbour_is_broken)
			*store << 1 << endl;
		else
			*store << 0 << endl;
	}
	*store << "</DataArray>" << endl;

	*store << "<DataArray type=\"Int64\" Name=\"1st_neig_fracture\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;
	for (int i = 0; i < nb_of_particles; i++)
	{
		if (particle_list[i].is_broken)
			*store << 1 << endl;
		else
			*store << 0 << endl;
	}
	*store << "</DataArray>" << endl;

    *store << "<DataArray type=\"Int64\" Name=\"general fracture\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;
    for (int i = 0; i < nb_of_particles; i++)
    {
        if (particle_list[i].is_broken || particle_list[i].second_neighbour_is_broken)
            *store << 1 << endl;
        else
            *store << 0 << endl;
    }
    *store << "</DataArray>" << endl;
}   

void Model::VTK_SavePointData_YoungsModulus(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"young\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;
    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].young << endl;
    }
    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_ParticleNumber(ofstream *store)
{
    *store << "<DataArray type=\"Int64\" Name=\"ParticleNumber\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].nb << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_X_Pos(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"x_pos\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].pos[0] << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Y_Pos(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"y_pos\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].pos[1] << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Z_Pos(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"z_pos\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].pos[2] << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_X_Layer(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"x-layer\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].xlayer << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Y_Layer(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"y-layer\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].ylayer << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Z_Layer(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"z-layer\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].zlayer << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Fix_X(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"fix_x\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].fix_x << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Fix_Y(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"fix_y\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].fix_y << endl;
    }

    *store << "</DataArray>" << endl;
}

void Model::VTK_SavePointData_Fix_Z(ofstream *store)
{
    *store << "<DataArray type=\"Float64\" Name=\"fix_z\" NumberOfComponents=\"1\" format=\"ascii\">" << endl;

    for (int i = 0; i < nb_of_particles; i++)
    {
        *store << particle_list[i].fix_z << endl;
    }

    *store << "</DataArray>" << endl;
}
