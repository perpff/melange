/*
  Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

  This file is part of Melange.

  Melange is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  Melange is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "deformation.h"

/***************************
 * every deformation-function MUST
 * take care of the following parameters
 * to be fully functional:
 * - model->x/y/zlength 				(whatever was changed)
 * - model->wrapping_constant_x/y		(if the respective system-dimension was changed)
 * - model->rep_box and its parameters	(by calling model->MakeRepBox())
 * *************************/

//using namespace NEWMAT;
using namespace std;

Deformation::Deformation(Model *mod)
{
  model = mod;
}

Deformation::~Deformation()
{
  //dtor
}

void Deformation::DeformLatticeSimpleShear(Real z)
{
  Real xpos;

  for (int i=0; i < model->nb_of_particles; i++)
    {
	  if (model->particle_list[i].is_left_boundary || model->particle_list[i].is_right_boundary)
		{
		  model->particle_list[i].fix_z = true;
		}
	}
	
  for (int i=0; i < model->nb_of_particles; i++)
    {
	  xpos = model->particle_list[i].pos[0];
		
	  model->particle_list[i].pos[2] += z * (1.0 - xpos / model->xlength);
    }
}

void Deformation::DeformLatticeWingCrack(Real y)
{
  int i;

  // y-movements
  if (y != 0.0)
    {	

	  cout << endl << "!!! Wrapping must be disabled for wing cracking !!! " <<endl;

	  for (i = 0; i < model->nb_of_particles; i++)
		{
		  model->particle_list[i].pos[1] *= (model->ylength + y) / model->ylength;
		}
		
	  model->ylength += y;
	}

  for (i=0; i < model->nb_of_particles; i++)
	{
	  model->particle_list[i].xlength = model->xlength;
	  model->particle_list[i].ylength = model->ylength;
	  model->particle_list[i].zlength = model->zlength;
	}
    
  cout << endl;
  cout << "Y-strain: " << (model->ylength - model->ylength_initial) / model->ylength_initial << endl;
  cout << "X-, Y-, Z-length: " << model->xlength << ", " << model->ylength << ", " << model->zlength << endl;
  cout << endl;
    
  model->MakeRepBox();
}

void Deformation::DeformLattice(Real x, Real z)
{
  int i;
    
  // adapt the y-length of the model to the actual height (plus some more)
  // since ylength is not confined in the model
  model->ylength = 0.0;
  for (i=0; i < model->nb_of_particles; i++)
    {
	  if (model->particle_list[i].pos[1] > (model->ylength - model->particle_radius))
		{
		  model->ylength = model->particle_list[i].pos[1] + model->particle_radius;
		}
	}

  // x-movements
  if (x != 0.0)
    {		
	  //~ model->wrapping_constant_x = model->nb_of_particles_in_x*2.0*model->particle_radius * ((model->xlength+x)/model->xlength_initial);
	  model->wrapping_constant_x *= (model->xlength + x) / model->xlength;
		
	  cout << model->wrapping_constant_x << " = model->wrapping_constant_x" <<endl;
	  for (i=0; i < model->nb_of_particles; i++)
		{
		  model->particle_list[i].wrapping_constant_x = model->wrapping_constant_x;
		}

	  for (i = 0; i < model->nb_of_particles; i++)
		{
		  model->particle_list[i].pos[0] *= (model->xlength + x) / model->xlength;
		}
		
	  model->xlength += x;
	}
    
  // z-movements
  if (z != 0.0)
    {
	  model->wrapping_constant_z *= ((model->zlength + z) / (model->zlength));
	  for (i=0; i < model->nb_of_particles; i++)
		{
		  model->particle_list[i].wrapping_constant_z = model->wrapping_constant_z;
		}

	  for (i = 0; i < model->nb_of_particles; i++)
		{
		  model->particle_list[i].pos[2] *= (model->zlength + z) / (model->zlength);
		}
		
	  model->zlength += z;
	}
	
  for (i=0; i < model->nb_of_particles; i++)
	{
	  model->particle_list[i].xlength = model->xlength;
	  model->particle_list[i].ylength = model->ylength;
	  model->particle_list[i].zlength = model->zlength;
	}
    
  cout << endl;
  cout << "X-strain: " << (model->xlength - model->xlength_initial) / model->xlength_initial << endl;
  cout << "Z-strain: " << (model->zlength - model->zlength_initial) / model->zlength_initial << endl;
  cout << "X-, Y-, Z-length: " << model->xlength << ", " << model->ylength << ", " << model->zlength << endl;
  cout << endl;
    
  model->MakeRepBox();
}

void Deformation::DeformLatticeSandbox(Real x, Real height, Real left, Real right)
{

  int i;
  Real xpos;
  Real rb = 0.0, lb = 0.0;
	
  for (i=0; i < model->nb_of_particles; i++)
	{
	  if (model->particle_list[i].is_right_boundary)
		{
		  rb = model->particle_list[i].pos[0];
		  break;
		}
	}
	
  for (i=0; i < model->nb_of_particles; i++)
	{
	  if (model->particle_list[i].is_left_boundary)
		{
		  lb = model->particle_list[i].pos[0];
		  break;
		}
	}
	
  // x-movements
  if (x != 0.0)
    {

	  left *= model->xlength / model->xlength_initial;
	  right *= model->xlength / model->xlength_initial;
		
	  for (i=0; i < model->nb_of_particles; i++)
		{
		  if (model->particle_list[i].pos[1] < height)
			{
			  if (model->particle_list[i].pos[0] - model->particle_radius >= left && model->particle_list[i].pos[0] - model->particle_radius <= right)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + (xpos-left) / (right-left) * x;
				}
			  else if (model->particle_list[i].pos[0] > right)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + rb*((model->xlength + x) / model->xlength - 1.0);
				}
			  else if (model->particle_list[i].pos[0] < left)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + lb*((model->xlength + x) / model->xlength - 1.0);
				}
		    }
		  else
			{
			  model->particle_list[i].pos[0] *= (model->xlength + x)/model->xlength;
			}
	   	}
	   	
	  model->wrapping_constant_x = model->wrapping_constant_x + (model->wrapping_constant_x/model->xlength)*x;
		
	  model->xlength += x;
	  cout << endl;
	  cout << "X-strain: " << (model->xlength - model->xlength_initial) / model->xlength_initial << endl;
	  cout << "model->wrapping_constant_x = "  << model->wrapping_constant_x << endl;
		
	}

  for (i=0; i < model->nb_of_particles; i++)
    {
	  model->particle_list[i].xlength = model->xlength;
    }

  model->MakeRepBox();
}

void Deformation::DeformLatticeXMid2(Real x, Real height, Real left1, Real right1, Real left2, Real right2)
{

  int i;
  Real xpos;
  Real rb = 0.0, lb = 1.0;
	
  for (i=0; i<model->nb_of_particles; i++)
	{
	  if (model->particle_list[i].is_left_boundary)
		if (model->particle_list[i].pos[0] < lb)
		  lb = model->particle_list[i].pos[0];

	  if (model->particle_list[i].is_right_boundary)
		if (model->particle_list[i].pos[0] > rb)
		  rb = model->particle_list[i].pos[0];
	}
	
  Real x_uncorrected = x;
  x = x * (rb - lb) / model->xlength;

  // x-movements
  if (x != 0.0)
    {
	  for (i=0; i < model->nb_of_particles; i++)
		{
		  if ( model->particle_list[i].ylayer > height )
			{
			  if ( model->particle_list[i].pos[0] / model->xlength >= left1 && model->particle_list[i].pos[0] / model->xlength <= right1 )
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + (xpos-left1) / (right1-left1) * x/2.0;
				}
			  else if ( model->particle_list[i].pos[0] / model->xlength >= left2 && model->particle_list[i].pos[0] / model->xlength <= right2 )
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = x/2.0 + xpos + (xpos-left1) / (right1-left1) * x/2.0;
				}
			  else if (model->particle_list[i].pos[0] / model->xlength > right1 && model->particle_list[i].pos[0] / model->xlength < left2)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + x/2.0;
				}
			  else if (model->particle_list[i].pos[0] / model->xlength > right2 )
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + x;
				}
		    }
		  else
			{
			  xpos = model->particle_list[i].pos[0];
			  model->particle_list[i].pos[0] = xpos + (xpos - lb) / (rb - lb) * x;
			}
	   	}
	   	
	  model->wrapping_constant_x = model->wrapping_constant_x + (model->wrapping_constant_x/model->xlength)*x_uncorrected;
		
	  model->xlength += x_uncorrected;
	  cout << endl;
	  cout << "X-strain: " << (model->xlength - model->xlength_initial) / model->xlength_initial << endl;
	  cout << "model->wrapping_constant_x = "  << model->wrapping_constant_x << endl;
		
	}

  for (i=0; i < model->nb_of_particles; i++)
    {
	  model->particle_list[i].xlength = model->xlength;
    }

  model->MakeRepBox();
    
}

void Deformation::DeformLatticeXMid(Real x, Real height, Real left, Real right)
{

  int i;
  Real xpos;
  Real rb = 0.0, lb = 0.0;
	
  for (i=0; i < model->nb_of_particles; i++)
	{
	  if (model->particle_list[i].is_right_boundary)
		{
		  rb = model->particle_list[i].pos[0];
		  break;
		}
	}
	
  for (i=0; i < model->nb_of_particles; i++)
	{
	  if (model->particle_list[i].is_left_boundary)
		{
		  lb = model->particle_list[i].pos[0];
		  break;
		}
	}
	
  // x-movements
  if (x != 0.0)
    {
	  left *= model->xlength / model->xlength_initial;
	  right *= model->xlength / model->xlength_initial;
		
	  for (i=0; i < model->nb_of_particles; i++)
		{
		  if (model->particle_list[i].pos[1] > height)
			{
			  if (model->particle_list[i].pos[0] - model->particle_radius >= left && model->particle_list[i].pos[0] - model->particle_radius <= right)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + (xpos-left) / (right-left) * x;
				}
			  else if (model->particle_list[i].pos[0] > right)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + rb*((model->xlength + x) / model->xlength - 1.0);
				}
			  else if (model->particle_list[i].pos[0] < left)
				{
				  xpos = model->particle_list[i].pos[0];
				  model->particle_list[i].pos[0] = xpos + lb*((model->xlength + x) / model->xlength - 1.0);
				}
		    }
		  else
			{
			  model->particle_list[i].pos[0] *= (model->xlength + x)/model->xlength;
			}
	   	}
	   	
	  model->wrapping_constant_x = model->wrapping_constant_x + (model->wrapping_constant_x/model->xlength)*x;
		
	  model->xlength += x;
	  cout << endl;
	  cout << "X-strain: " << (model->xlength - model->xlength_initial) / model->xlength_initial << endl;
	  cout << "model->wrapping_constant_x = "  << model->wrapping_constant_x << endl;
		
	}

  for (i=0; i < model->nb_of_particles; i++)
    {
	  model->particle_list[i].xlength = model->xlength;
    }

  model->MakeRepBox();
    
}

void Deformation::VerticallyIncreasingXStrain(Real maxx, Real min_height, Real max_height)
{

  Particle *prtcl;

  Real maxy = 0.0, miny = 100.0, Dy, ypos, xmove;

  for (int i=0; i < model->nb_of_particles; i++)
	{

	  prtcl = &(model->particle_list[i]);

	  if (prtcl->pos[1] > min_height && prtcl->pos[1] < max_height)
		{
		  if (prtcl->pos[1] > maxy)
			maxy = prtcl->pos[1];
		  if (prtcl->pos[1] < miny)
			miny = prtcl->pos[1];
		}
	  else if (prtcl->pos[1] > max_height)
		prtcl->fix_x = false;
	}

  Dy = maxy - miny;

  for (int i=0; i < model->nb_of_particles; i++)
	{
	  prtcl = &(model->particle_list[i]);

	  if (prtcl->pos[1] > min_height && prtcl->pos[1] < max_height)
		{
		  ypos = prtcl->pos[1] - miny;
		  xmove = (ypos / Dy) * maxx;
					
		  prtcl->pos[0] *= (model->xlength + xmove) / model->xlength;
		  prtcl->pos[0] += (maxx - maxx * (ypos / Dy)) / 2.0;
		}
	  else if (prtcl->pos[1] < min_height)
		prtcl->pos[0] *= (model->xlength + maxx) / model->xlength;
	}

  model->xlength += maxx;

  model->MakeRepBox();

}
