/*
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "relaxation.h"

using namespace std;

Relaxation::Relaxation(Model *mod)
{
    model = mod;
    relaxthreshold = 1e-9;
    fover = 1.8;
    dynamic_relax_thresh = false;
    
    srand ( time(NULL) );
}

Relaxation::~Relaxation()
{
    //dtor
}

void Relaxation::ActivateDynamicORF(bool activate)
{
    this->dynamic_relax_thresh  = activate;

    for (int i = 0; i < model->nb_of_particles; i++)
    {
        model->particle_list[i].dynamic_relax_thresh = activate;
    }
}

void Relaxation::SetRelaxationThreshold(Real relaxthreshold)
{
    this->relaxthreshold = relaxthreshold;

    for (int i = 0; i < model->nb_of_particles; i++)
    {
        model->particle_list[i].relaxthreshold = relaxthreshold;
    }
}

void Relaxation::SetOverRelaxationFactor(Real fover)
{
    this->fover = fover;

    for (int i = 0; i < model->nb_of_particles; i++)
    {
        model->particle_list[i].fover = fover;
    }
}

void Relaxation::Relax()
{

    Particle *save_partcl = NULL;
    Real maxbreak = 0.0;
    int i, nb_of_particles = model->nb_of_particles;

    while (FullRelax ())
    {

        maxbreak = 0.0;
        save_partcl = NULL;

        for (i = 0; i < nb_of_particles; i++)
        {
            if (model->particle_list[i].mbreak > maxbreak)
            {
                maxbreak = model->particle_list[i].mbreak;
                save_partcl = &model->particle_list[i];
            }
        }

        if (save_partcl)
            std::cout << "Broken bond! No of particle: " << save_partcl->nb << " No of neigh: " << save_partcl->neigh << std::endl;
        save_partcl->BreakBond ();

    }
	
    if (model->viscoelastic)
    {		
		// remove unnecessary unconnected particles from list
		if ( model->repulsion )
		{
			for (i=0; i < model->nb_of_particles; i++)
			{
				model->particle_list[i].visc_unconnected_neigh_list.remove_if(RemoveParticleFromList());
			}
		}

		// Maxwell viscoelasticity for particles
		ViscousFlow();

		// a second relaxation, to find the new elastic equilibrium after viscous particle deformation
		while (FullRelax ())
		{

		    maxbreak = 0.0;
		    save_partcl = NULL;

		    for (i = 0; i < nb_of_particles; i++)
		    {
		        if (model->particle_list[i].mbreak > maxbreak)
		        {
		            maxbreak = model->particle_list[i].mbreak;
		            save_partcl = &model->particle_list[i];
		        }
		    }

		    if (save_partcl)
		        std::cout << "Broken bond! No of particle: " << save_partcl->nb << " No of neigh: " << save_partcl->neigh << std::endl;
		    save_partcl->BreakBond ();

		}
	}
}

void Relaxation::ViscousFlow()
{

	cout << "viscous relaxation routine" << endl;
	
	// 1. die neue ellipse berechnen
	// 2. zuordnen der neuen spring-längen (1., 2. nachbar, + repulsion)
	
	Particle *prtcl;
	std::list<Particle::visc_unconnected_particle>::iterator it;
	
	// Real k, dx, dy, dz, abs;

	Real t = model->timestep;
	Real k1, k2, k3;
	Real h;
	Real tau;
	Real mean;

	Real dx, dy, dz, k, ab;
	
	// the principal strain tensor
	DiagonalMatrix S(3), D(3);
	// the rotation matrix
	Matrix R(3,3);
	// the shape matrix of the ellipsoid
	SymmetricMatrix E(3);
	Matrix C(3,3);
	ColumnVector p(3);

	for ( int i=0; i < model->nb_of_particles; i++)
	{
		prtcl = &(model->particle_list[i]);

		if (!(prtcl->is_bottom_boundary))
		{
			mean = (prtcl->sigma1 + prtcl->sigma2 + prtcl->sigma3) / 3.0;

			prtcl->viscosity = GetCurrentNewtonianViscosity(prtcl);

			tau = prtcl->viscosity / prtcl->real_young;

			h = exp(-t/tau)-1.0;

			// using the deviatoric principal stress tensor

			// the equations below are condensed
			// they are equal to the sigma/E-part
			// of the 'real' equations
			k1 = (prtcl->sigma1 - mean) / prtcl->real_young;
			k2 = (prtcl->sigma2 - mean) / prtcl->real_young;
			k3 = (prtcl->sigma3 - mean) / prtcl->real_young;

			// This is not a real 'plastic' yield stress, 
			// but wants to protect against random fluctuations
			// caused by numerical errors.
			// can be easily extended, though!
			if ( prtcl->diff_stress > 1e7 )
			{
				R.element(0,0) = prtcl->sigma1_vec[0];
				R.element(1,0) = prtcl->sigma1_vec[1];
				R.element(2,0) = prtcl->sigma1_vec[2];

				R.element(0,1) = prtcl->sigma2_vec[0];
				R.element(1,1) = prtcl->sigma2_vec[1];
				R.element(2,1) = prtcl->sigma2_vec[2];

				R.element(0,2) = prtcl->sigma3_vec[0];
				R.element(1,2) = prtcl->sigma3_vec[1];
				R.element(2,2) = prtcl->sigma3_vec[2];	
				
				R = R.i();
				
				// we MAKE THE EXPONENT NEGATIVE because the deformation matrix 
				// for the ellipsoid requires the INVERSE of the stretch 
				S.element(0) = exp(- k1 * h);
				S.element(1) = exp(- k2 * h);
				S.element(2) = exp(- k3 * h);
				
				// this describes the ellipsoid (repulsion)
				prtcl->T = R.i() * S * R * prtcl->T;

				// Note: in newmat '<<' instead of '=' must be used if information 
				// is lost in the conversion between different matrix types. 
				E << prtcl->T * prtcl->T.t();
				
				// symmetric matrices in newmat use the lower triangle
				prtcl->e11 = E.element(0,0);
				prtcl->e21 = E.element(1,0);
				prtcl->e22 = E.element(1,1);
				prtcl->e31 = E.element(2,0);
				prtcl->e32 = E.element(2,1);
				prtcl->e33 = E.element(2,2);
				
				// now the deformation matrix for the particle movement (positive exponent)
				S.element(0) = exp(k1 * h);
				S.element(1) = exp(k2 * h);
				S.element(2) = exp(k3 * h);
				C = R.i() * S * R;

				for (int j=0; j<12; j++)
				{
					if (prtcl->neighbour_list[j])
					{
						
						p.element(0) = prtcl->neig_hull_intersect[j][0];
						p.element(1) = prtcl->neig_hull_intersect[j][1];
						p.element(2) = prtcl->neig_hull_intersect[j][2];

						p = C * p;
						
						prtcl->neig_hull_intersect[j][0] = p.element(0);
						prtcl->neig_hull_intersect[j][1] = p.element(1);
						prtcl->neig_hull_intersect[j][2] = p.element(2);
					}
				}

				for (int j=0; j<6; j++)
				{
					if (prtcl->second_neighbour_list[j])
					{
						
						p.element(0) = prtcl->sec_neig_hull_intersect[j][0];
						p.element(1) = prtcl->sec_neig_hull_intersect[j][1];
						p.element(2) = prtcl->sec_neig_hull_intersect[j][2];

						p = C * p;
						
						prtcl->sec_neig_hull_intersect[j][0] = p.element(0);
						prtcl->sec_neig_hull_intersect[j][1] = p.element(1);
						prtcl->sec_neig_hull_intersect[j][2] = p.element(2);
					}
				}
			}
		}
	}

	Real p1[3], p2[3];
	Particle *neig;

	for ( int i=0; i < model->nb_of_particles; i++)
	{
		prtcl = &(model->particle_list[i]);

		for (int j=0; j<12; j++)
		{
			if (prtcl->neighbour_list[j])
			{

				neig = prtcl->neighbour_list[j];

				p1[0] = prtcl->neig_hull_intersect[j][0];
				p1[1] = prtcl->neig_hull_intersect[j][1];
				p1[2] = prtcl->neig_hull_intersect[j][2];

				p2[0] = neig->neig_hull_intersect[ neig->neig_back_ptr[j] ][0];
				p2[1] = neig->neig_hull_intersect[ neig->neig_back_ptr[j] ][1];
				p2[2] = neig->neig_hull_intersect[ neig->neig_back_ptr[j] ][2];

				prtcl->neigpos[j][0] = p1[0] - p2[0];
				prtcl->neigpos[j][1] = p1[1] - p2[1];
				prtcl->neigpos[j][2] = p1[2] - p2[2];
			}
		} 

		for (int j=0; j<6; j++)
		{
			if (prtcl->second_neighbour_list[j])
			{

				neig = prtcl->second_neighbour_list[j];

				p1[0] = prtcl->sec_neig_hull_intersect[j][0];
				p1[1] = prtcl->sec_neig_hull_intersect[j][1];
				p1[2] = prtcl->sec_neig_hull_intersect[j][2];

				p2[0] = neig->sec_neig_hull_intersect[ neig->sec_neig_back_ptr[j] ][0];
				p2[1] = neig->sec_neig_hull_intersect[ neig->sec_neig_back_ptr[j] ][1];
				p2[2] = neig->sec_neig_hull_intersect[ neig->sec_neig_back_ptr[j] ][2];

				prtcl->secneigpos[j][0] = p1[0] - p2[0];
				prtcl->secneigpos[j][1] = p1[1] - p2[1];
				prtcl->secneigpos[j][2] = p1[2] - p2[2];
			}
		}
	}

	// sync to the particle shape 
	for ( int i=0; i < model->nb_of_particles; i++)
	{
		prtcl = &(model->particle_list[i]);

		for (int j=0; j<12; j++)
		{
			if (prtcl->neighbour_list[j])
			{
		        dx = prtcl->neigpos[j][0];
		        dy = prtcl->neigpos[j][1];
		        dz = prtcl->neigpos[j][2];
		        ab = sqrt (dx*dx + dy*dy + dz*dz);
		        dx /= ab;
		        dy /= ab;
		        dz /= ab;
		        
		        k = sqrt(1.0/(prtcl->e11*dx*dx + 2.0*prtcl->e21*dx*dy + 2.0*prtcl->e31*dx*dz + prtcl->e22*dy*dy + 2.0*prtcl->e32*dy*dz + prtcl->e33*dz*dz));
		        k = k * prtcl->radius;

		        prtcl->neig_hull_intersect[j][0] = k * dx;
		        prtcl->neig_hull_intersect[j][1] = k * dy;
		        prtcl->neig_hull_intersect[j][2] = k * dz;
			}
		}

		for (int j=0; j<6; j++)
		{
			if (prtcl->second_neighbour_list[j])
			{
		        dx = prtcl->secneigpos[j][0];
		        dy = prtcl->secneigpos[j][1];
		        dz = prtcl->secneigpos[j][2];
		        ab = sqrt (dx*dx + dy*dy + dz*dz);
		        dx /= ab;
		        dy /= ab;
		        dz /= ab;
		        
		        k = sqrt(1.0/(prtcl->e11*dx*dx + 2.0*prtcl->e21*dx*dy + 2.0*prtcl->e31*dx*dz + prtcl->e22*dy*dy + 2.0*prtcl->e32*dy*dz + prtcl->e33*dz*dz));
		        k = k * prtcl->second_neig_radius;

		        prtcl->sec_neig_hull_intersect[j][0] = k * dx;
		        prtcl->sec_neig_hull_intersect[j][1] = k * dy;
		        prtcl->sec_neig_hull_intersect[j][2] = k * dz;
			}
		}
	}

	// the repulsion-alen has to be calculated in a separate loop, because the T-Matrix of the neighbors needs to be known first
	if (model->repulsion)
	{
		for ( int i=0; i < model->nb_of_particles; i++)
		{
			prtcl = &(model->particle_list[i]);

			for (it=prtcl->visc_unconnected_neigh_list.begin(); it!=prtcl->visc_unconnected_neigh_list.end(); ++it)
			{
				(*it).alen = prtcl->GetRepAlen((*it).neig);
			}
		}
	}
}

Real Relaxation::GetCurrentNewtonianViscosity(Particle *prtcl)
{
	Real apparent_viscosity;

	if (prtcl->power_law_viscosity)
	{ 
		apparent_viscosity = prtcl->A * pow(prtcl->diff_stress, prtcl->n);
		apparent_viscosity = apparent_viscosity * exp ( -(prtcl->Ea + prtcl->Va * prtcl->diff_stress) / (gas_constant * prtcl->temperature) );
		apparent_viscosity = prtcl->diff_stress / apparent_viscosity;

		// cout << apparent_viscosity << endl;

		return (apparent_viscosity);
	}
	else
		return (prtcl->viscosity);
}

bool Relaxation::FullRelax()
{
    int nb_of_particles = model->nb_of_particles;
    bool relaxflag, breakBond = false;

    //TODO: bessere Speicherverwaltung
    //das sollte bei gelegenheit in eine bessere
    //speicherverwaltung umgewandelt werden,
    //insbesondere solten die Zeiger-Arrays
    //am Beginn für jeden Processor angelegt werden

    Particle* particle_list = &(model->particle_list[0]);
    // for parallelization
	int threadnum;
	
    do
    {
		relaxflag = false;
		
		#pragma omp parallel firstprivate(particle_list) private (threadnum)
		{
			threadnum = omp_get_thread_num();
			
			#pragma omp for schedule(static)
			for (int i = 0; i < nb_of_particles; i++)
			{
				if (!RelaxModel(&particle_list[i], threadnum))
				{
					//	#pragma omp critical
					relaxflag = true;
				}
				UpdateParticlePos(&particle_list[i]);
				
			}
				
			if (model->repulsion)
			{
				#pragma omp for schedule(static)
				for (int i = 0; i < nb_of_particles; i++)
				{
					//#pragma omp critical
					model->ChangeBox(&particle_list[i]);
				}
			}
		}
	
    }
    while ( relaxflag );

    breakBond = false;

	//#pragma omp parallel firstprivate(particle_list) private (threadnum) 
	{
		threadnum = omp_get_thread_num();
		//#pragma omp for schedule(static)    
		for (int i = 0; i < nb_of_particles; i++)
    	{
	        if (CalcStressAndBrittleFailure(&particle_list[i], threadnum))
	            breakBond = true;
    	}
	}

    return breakBond;

}

bool Relaxation::CalcStressAndBrittleFailure(Particle *prtcl, int threadnum)
{

    /************************************************************

    axes/indices of the stress tensor are taken to be similar to
    the axes of the geometrical/opengl/model/particles coordinate
    system.

    xx xy xz
    yx yy yz
    zx zy zz

    Tensor is symmetric. See also notes on the program.

    *************************************************************/

    Real dx, dy, dz, dd;   // distances
    Real alen;             // equilibrium length
    Real uxi, uyi, uzi;    // geometrical helper variables (for sin/cos)
    Real fx, fy, fz, fn;   // (normal-)forces in x, y, z and the spring direction
    
    Particle *custom_neig_list[1500];

    Real ten_break;

    bool bbreak = false;

    // TODO: check if THIS IS CORRECT?
    const Real area = prtcl->radius * prtcl->radius * pi;

    int i, j, m;
    int x,y,z;
    int neig_count = 0;
    
    bool flag;
    
    Real f, c, ux, uy, uz, usx, usy, usz, unx, uny, unz, nx, ny, nz;
    Real ds;
    Real G, E, v; 		// shear modulus, average Youngs modulus, average Poisson factor
	Real shear_stress, normal_stress, mc;

	ColumnVector n(3);

    Particle *neig;
    std::list<Particle::visc_unconnected_particle>::iterator it;
	
    prtcl->done_for_multithreads[threadnum] = true;
    custom_neig_list[neig_count++] = prtcl;

    prtcl->sxx = 0.0;
    prtcl->syy = 0.0;
    prtcl->szz = 0.0;

    prtcl->sxy = 0.0;
    prtcl->sxz = 0.0;
    prtcl->syz = 0.0;

    prtcl->mbreak = 0.0;

    if (prtcl->is_second_neighbour_model)
    {
		// connected first neighbours
		for (i = 0; i < 12; i++)
		{
			if (prtcl->neighbour_list[i])
			{

	            dx = prtcl->GetXDist(prtcl->neighbour_list[i]);
	            dy = prtcl->neighbour_list[i]->pos[1] - prtcl->pos[1];
	            dz = prtcl->GetZDist(prtcl->neighbour_list[i]);
	            
	            ux = dx - prtcl->neigpos[i][0];
				uy = dy - prtcl->neigpos[i][1];
				uz = dz - prtcl->neigpos[i][2];
	            
	            dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));

	            alen = sqrt(prtcl->neigpos[i][0]*prtcl->neigpos[i][0] + prtcl->neigpos[i][1]*prtcl->neigpos[i][1] + prtcl->neigpos[i][2]*prtcl->neigpos[i][2]);

	            if (dd != 0.0)
	            {
					nx = dx / dd;
					ny = dy / dd;
					nz = dz / dd;
				}
				else
				{
					nx = 0;
					ny = 0;
					nz = 0;
				}
				
				f = ux*nx + uy*ny + uz*nz;
				unx = nx * f;
				uny = ny * f;
				unz = nz * f;
				
				usx = ux - unx;
				usy = uy - uny;
				usz = uz - unz;
				
				// *** normal force
				c = (prtcl->k1 + prtcl->neighbour_list[i]->k1) / 2.0;
	            fx = unx * c;
	            fy = uny * c;
	            fz = unz * c;
	            
				// the stress tensor	       
				prtcl->sxx += nx * fx;
				prtcl->syy += ny * fy;
				prtcl->szz += nz * fz;

				prtcl->sxy += nx * fy;
				prtcl->sxz += nx * fz;
				prtcl->syz += ny * fz;
	            
	            // *** shear force				
				c = (prtcl->ks1 + prtcl->neighbour_list[i]->ks1) / 2.0;
	            fx = usx * c;
	            fy = usy * c;
	            fz = usz * c;
	            
				// the stress tensor
				prtcl->sxx += nx * fx;
				prtcl->syy += ny * fy;
				prtcl->szz += nz * fz;
				
				prtcl->sxy += nx * fy;
				prtcl->sxz += nx * fz;
				prtcl->syz += ny * fz;
				
				// the next part is the spring based bond breaking routine
				if (!prtcl->stress_tensor_based_failure)
				{
					ds = sqrt(usx*usx + usy*usy + usz*usz);

					// Young's modulus
					E = (prtcl->real_young + prtcl->neighbour_list[i]->real_young) / 2.0;
					// Poisson ratio
					v = (prtcl->v + prtcl->neighbour_list[i]->v) / 2.0;
					// Shear modulus
					G = E / (2.0 * (1.0 + v));
					// shear strength (see Paper: ...)
					mc = -(prtcl->break_Str[i] * (1.0 + sin(prtcl->angle_of_internal_friction))); 
					mc = mc / (2.0 * cos(prtcl->angle_of_internal_friction));

					normal_stress = (alen - dd) / alen * E;

					if (normal_stress < 0.0) {
						ten_break = normal_stress / prtcl->break_Str[i];
					}
					else {
						ten_break = 0.0;
					}
					
					if (ten_break > 1.0)
					{
						if (ten_break > prtcl->mbreak)
						{
							if (!prtcl->no_break[i])
							{
								prtcl->mbreak = ten_break;

								prtcl->neigh = i;
								bbreak = true;
							}
						}
					}

					if (model->mohr_coulomb_failure)
					{
						// shear stress
						shear_stress = fabs(G * ds / alen);

						// probability:
						if (normal_stress >= 0)
							ten_break = shear_stress / (mc + normal_stress*tan(prtcl->angle_of_internal_friction));
						else
							ten_break = shear_stress / mc;
					
						if (ten_break > 1.0)
						{


							if (ten_break > prtcl->mbreak)
							{
								if (!prtcl->no_break[i])
								{
									prtcl->mbreak = ten_break;

									prtcl->neigh = i;
									bbreak = true;
								}
							}
						}	

					}
				}
				
				prtcl->neighbour_list[i]->done_for_multithreads[threadnum] = true;
				custom_neig_list[neig_count++] = prtcl->neighbour_list[i];
			}
		}

		// connected second neighbours
	    for (i = 0; i < 6; i++)
	    {
	        if (prtcl->second_neighbour_list[i])
	        {
	
	            dx = prtcl->GetXDist(prtcl->second_neighbour_list[i]);
	            dy = prtcl->second_neighbour_list[i]->pos[1] - prtcl->pos[1];
	            dz = prtcl->GetZDist(prtcl->second_neighbour_list[i]);
	            
	            ux = dx - prtcl->secneigpos[i][0];
				uy = dy - prtcl->secneigpos[i][1];
				uz = dz - prtcl->secneigpos[i][2];
	            
	            dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));
	            alen = sqrt(prtcl->secneigpos[i][0]*prtcl->secneigpos[i][0] + prtcl->secneigpos[i][1]*prtcl->secneigpos[i][1] + prtcl->secneigpos[i][2]*prtcl->secneigpos[i][2]);

	            if (dd != 0.0)
	            {
					nx = dx / dd;
					ny = dy / dd;
					nz = dz / dd;
				}
				else
				{
					nx = 0;
					ny = 0;
					nz = 0;
				}
	
				f = ux*nx + uy*ny + uz*nz;
				unx = nx * f;
				uny = ny * f;
				unz = nz * f;
				
				usx = ux - unx;
				usy = uy - uny;
				usz = uz - unz;
				
				// *** normal force
				c = (prtcl->k2 + prtcl->second_neighbour_list[i]->k2) / 2.0;
	            fx = unx * c;
	            fy = uny * c;
	            fz = unz * c;
	            
				// the stress tensor	       
				prtcl->sxx += nx * fx;
				prtcl->syy += ny * fy;
				prtcl->szz += nz * fz;

				prtcl->sxy += nx * fy;
				prtcl->sxz += nx * fz;
				prtcl->syz += ny * fz;
	            
	            // *** shear force				
				c = (prtcl->ks2 + prtcl->second_neighbour_list[i]->ks2) / 2.0;
	            fx = usx * c;
	            fy = usy * c;
	            fz = usz * c;
				
				// the stress tensor
				prtcl->sxx += nx * fx;
				prtcl->syy += ny * fy;
				prtcl->szz += nz * fz;

				prtcl->sxy += nx * fy;
				prtcl->sxz += nx * fz;
				prtcl->syz += ny * fz;

				// the next part is the spring based bond breaking routine
				if (!prtcl->stress_tensor_based_failure)
				{
					ds = sqrt(usx*usx + usy*usy + usz*usz);

					// Young's modulus
					E = (prtcl->real_young + prtcl->second_neighbour_list[i]->real_young) / 2.0;
					// Poisson ratio
					v = (prtcl->v + prtcl->second_neighbour_list[i]->v) / 2.0;
					// Shear modulus
					G = E / (2.0 * (1.0 + v));
					// shear strength (see Paper: ...)
					mc = -(prtcl->second_neigh_break_Str[i] * (1.0 + sin(prtcl->angle_of_internal_friction))); 
					mc = mc / (2.0 * cos(prtcl->angle_of_internal_friction));

					normal_stress = (alen - dd) / alen * E;

					if (normal_stress < 0.0) {
						ten_break = normal_stress / prtcl->second_neigh_break_Str[i];
					}
					else {
						ten_break = 0.0;
					}
					
					if (ten_break > 1.0)
					{
						if (ten_break > prtcl->mbreak)
						{
							if (!prtcl->second_neigh_no_break[i])
							{
								prtcl->mbreak = ten_break;

								prtcl->neigh = i + 12;
								bbreak = true;
							}
						}
					}

					if (model->mohr_coulomb_failure)
					{
						// shear stress
						shear_stress = fabs(G * ds / alen);

						// probability:
						if (normal_stress >= 0)
							ten_break = shear_stress / (mc + normal_stress*tan(prtcl->angle_of_internal_friction));
						else
							ten_break = shear_stress / mc;
					
						if (ten_break > 1.0)
						{
							if (ten_break > prtcl->mbreak)
							{
								if (!prtcl->second_neigh_no_break[i])
								{
									prtcl->mbreak = ten_break;

									prtcl->neigh = i + 12;
									bbreak = true;
								}
							}
						}	
					}
				}
			
	            prtcl->second_neighbour_list[i]->done_for_multithreads[threadnum] = true;
	            custom_neig_list[neig_count++] = prtcl->second_neighbour_list[i];
	        }
	    }
	}

    // for unconnected neighbours:
    // unconnected particles / repbox
    if (prtcl->repulsion)
    {
		if (prtcl->viscoelastic)
		{
			for (it=prtcl->visc_unconnected_neigh_list.begin(); it!=prtcl->visc_unconnected_neigh_list.end(); ++it)
			{
				neig = (*it).neig;
				
				if (!neig->done_for_multithreads[threadnum])
				{
					// first manage the rep-box
					neig->done_for_multithreads[threadnum] = true;
					custom_neig_list[neig_count++] = neig;
					
					dx = prtcl->GetXDist(neig);
					dy = (neig->pos[1] - prtcl->pos[1]);
					dz = prtcl->GetZDist(neig);
		
					dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));
		
					alen = (*it).alen;
						
					fn = ((prtcl->k1 + neig->k1) / 2.0) * (dd - alen);

					if (fn <= 0.0)
					{						
						if (dd != 0.0)
						{
							uxi = dx / dd;
							uyi = dy / dd;
							uzi = dz / dd;
						}
						else
						{
							uxi = 0.0;
							uyi = 0.0;
							uzi = 0.0;
						}
						
						fx = fn * uxi;
						fy = fn * uyi;
						fz = fn * uzi;
						
						// the stress tensor
						prtcl->sxx += uxi * fx;
						prtcl->syy += uyi * fy;
						prtcl->szz += uzi * fz;

						prtcl->sxy += uxi * fy;
						prtcl->sxz += uxi * fz;
						prtcl->syz += uyi * fz;
					}
					else
					{
						// the condition is a bit arbitrary but ensures that particels
						// don't get removed from the list to soon 
						if (dd > 2.0 * alen)
						{
							(*it).remove = true;
						}
					}
				}
			}
		}
		else
		{
			for (i=-1; i<=1; i++)
			{
				for (j=-1; j<=1; j++)
				{
					for (m=-1; m<=1; m++)
					{

						x = prtcl->box_pos[0] + i;
						y = prtcl->box_pos[1] + j;
						z = prtcl->box_pos[2] + m;
						
						neig = NULL;
						
						if ( !(y >= prtcl->repbox_dim_y || y < 0) )
						{
							
							flag = true;
							
							if ( x >= prtcl->repbox_dim_x || x < 0 )
							{
								if (prtcl->use_periodic_boundaries_in_x)
								{
									if (x >= prtcl->repbox_dim_x)
										x -= prtcl->repbox_dim_x;
									else if (x < 0)
										x += prtcl->repbox_dim_x;
								}
								else
									flag = false;
									
							}
							
							if (z >= prtcl->repbox_dim_z || z < 0)
							{
								if (prtcl->use_periodic_boundaries_in_z)
								{
									if (z >= prtcl->repbox_dim_z)
										z -= prtcl->repbox_dim_z;
									else if (z < 0)
										z += prtcl->repbox_dim_z;
								}
								else
									flag = false;
							}
							
							if ( flag )
								neig = model->rep_box[x][y][z];
						}
						
						while (neig)
						{
							if (!neig->done_for_multithreads[threadnum])
							{

								dx = prtcl->GetXDist(neig);
								dy = (neig->pos[1] - prtcl->pos[1]);
								dz = prtcl->GetZDist(neig);

								dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));

								alen = 2.0*prtcl->radius;

								// only replusive forces
								if (dd < alen)
								{

									if (dd != 0.0)
									{
										uxi = dx / dd;
										uyi = dy / dd;
										uzi = dz / dd;
									}
									else
									{
										uxi = 0.0;
										uyi = 0.0;
										uzi = 0.0;
									}

									fn = ((prtcl->k1 + neig->k1) / 2.0) * (dd - alen);
									fx = fn * uxi;
									fy = fn * uyi;
									fz = fn * uzi;

									// the stress tensor
									prtcl->sxx += uxi * fx;
									prtcl->syy += uyi * fy;
									prtcl->szz += uzi * fz;

									prtcl->sxy += uxi * fy;
									prtcl->sxz += uxi * fz;
									prtcl->syz += uyi * fz;
									
								}

								// manage the rep-box
								neig->done_for_multithreads[threadnum] = true;
								custom_neig_list[neig_count++] = neig;

							}
							neig = neig->next_in_box;
						}
					}
				}
			}
		}
	}
	
    // gravity??? NO! Only body-forces, no external forces for stress!
    // if (prtcl->use_gravity_force)
        // prtcl->syy -= ( prtcl->fg / (prtcl->radius*prtcl->radius*pi) );
	
    if (prtcl->walls_in_x)
    {
		if (prtcl->pos[0] < prtcl->radius)
			prtcl->sxx -= prtcl->radius * prtcl->k1 * (prtcl->pos[0] - prtcl->radius);
		else if (prtcl->pos[0] > (prtcl->xlength - prtcl->radius))
			prtcl->sxx -= prtcl->radius * prtcl->k1 * (prtcl->radius - (prtcl->xlength - prtcl->pos[0]));
	}

    if (prtcl->walls_in_z)
    {
		if (prtcl->pos[2] < prtcl->radius)
			prtcl->szz -= prtcl->radius * prtcl->k1 * (prtcl->pos[2] - prtcl->radius);
		else if (prtcl->pos[2] > (prtcl->zlength - prtcl->radius))
			prtcl->szz -= prtcl->radius * prtcl->k1 * (prtcl->radius - (prtcl->zlength - prtcl->pos[2]));
	}

    // unset the 'done'-flags
    for (i=0; i<neig_count; i++) custom_neig_list[i]->done_for_multithreads[threadnum] = false;

    // IS THIS CORRECT ??????????????
    prtcl->sxx = prtcl->sxx / area;
    prtcl->syy = prtcl->syy / area;
    prtcl->szz = prtcl->szz / area;

    prtcl->sxy = prtcl->sxy / area;
    prtcl->sxz = prtcl->sxz / area;
    prtcl->syz = prtcl->syz / area;
    
    // correct the sign:
    prtcl->sxx *= -1.0;
    prtcl->syy *= -1.0;
    prtcl->szz *= -1.0;

    prtcl->sxy *= -1.0;
    prtcl->sxz *= -1.0;
    prtcl->syz *= -1.0;
    
    // correct for the "real" young's modulus
    prtcl->sxx *= (prtcl->real_young/prtcl->young) ;
    prtcl->syy *= (prtcl->real_young/prtcl->young);
    prtcl->szz *= (prtcl->real_young/prtcl->young);

    prtcl->sxy *= (prtcl->real_young/prtcl->young);
    prtcl->sxz *= (prtcl->real_young/prtcl->young);
    prtcl->syz *= (prtcl->real_young/prtcl->young);   
	
	/*
	* below we calculate the principal stresses:
	* value and direction
	* using the eigenvalue / eigenvector of the stress-tensor
	*/
	
	SymmetricMatrix sigma(3);
	Matrix V;
	DiagonalMatrix D;
	
	sigma.element(0,0) = prtcl->sxx;
	sigma.element(1,1) = prtcl->syy;
	sigma.element(2,2) = prtcl->szz;
	
	sigma.element(1,0) = prtcl->sxy;
	sigma.element(2,0) = prtcl->sxz;
	sigma.element(2,1) = prtcl->syz;
	
	EigenValues(sigma, D, V);
	
	// get sigmax
	prtcl->sigma1 = D.element(2);
	prtcl->sigma2 = D.element(1);
	prtcl->sigma3 = D.element(0);
	
	// read columns and put into sigmax_vec
	for (int i=0; i<3; i++)
		prtcl->sigma1_vec[i] = V.element (i,2);
	for (int i=0; i<3; i++)
		prtcl->sigma2_vec[i] = V.element (i,1);
	for (int i=0; i<3; i++)
		prtcl->sigma3_vec[i] = V.element (i,0);
	
	prtcl->mean_stress = (prtcl->sxx + prtcl->syy + prtcl->szz) / 3.0;
	prtcl->diff_stress = prtcl->sigma1 - prtcl->sigma3;
	
	// stress tensor based fracturing
	if (prtcl->stress_tensor_based_failure)
	{
		for (int i=0; i<12; i++)
		{
			if (prtcl->neighbour_list[i])
			{
				if (!prtcl->no_break[i])
				{			

					dx = prtcl->neigpos[i][0];
					dy = prtcl->neigpos[i][1];
					dz = prtcl->neigpos[i][2];
		            
					dd = sqrt(dx*dx + dy*dy + dz*dz);
					n.element(0) = dx/dd;
					n.element(1) = dy/dd;
					n.element(2) = dz/dd;
					
					normal_stress = DotProduct(n, sigma*n);
					shear_stress = fabs((sigma*n).Norm1() - normal_stress);

					// constant for the mohr-coulomb criterion
					mc = (prtcl->break_Str[i] * (1.0 + sin(prtcl->angle_of_internal_friction))); 
					mc = mc / (2.0 * cos(prtcl->angle_of_internal_friction));
					
					if (normal_stress < 0.0) 
					{
						ten_break = normal_stress / prtcl->break_Str[i];

						if (ten_break > 1.0)
						{
							if (ten_break > prtcl->mbreak)
							{
								prtcl->mbreak = ten_break;
								prtcl->neigh = i;            // numbers >= 12 indicated a second neigbour
								bbreak = true;
							}
						}
					}
					else if (normal_stress >= 0.0 && model->mohr_coulomb_failure)
					{
						ten_break = shear_stress / (mc + normal_stress * tan(prtcl->angle_of_internal_friction));

						if (ten_break > 1.0)
						{
							if (ten_break > prtcl->mbreak)
							{
								prtcl->mbreak = ten_break;
								prtcl->neigh = i;            // numbers >= 12 indicated a second neigbour
								bbreak = true;
							}
						}
					}
					else {
						ten_break = 0.0;
					}
				}
			}
		}

		for (int i=0; i<6; i++)
		{

			if (prtcl->second_neighbour_list[i])
			{

				if (!prtcl->second_neigh_no_break[i])
				{
					
					dx = prtcl->secneigpos[i][0];
					dy = prtcl->secneigpos[i][1];
					dz = prtcl->secneigpos[i][2];

					dd = sqrt(dx*dx + dy*dy + dz*dz);
					n.element(0) = dx/dd;
					n.element(1) = dy/dd;
					n.element(2) = dz/dd;
					
					normal_stress = DotProduct(n, sigma*n);
					shear_stress = fabs((sigma*n).Norm1() - normal_stress);

					// constant for the mohr-coulomb criterion
					mc = (prtcl->second_neigh_break_Str[i] * (1.0 + sin(prtcl->angle_of_internal_friction))); 
					mc = mc / (2.0 * cos(prtcl->angle_of_internal_friction));
					
					if (normal_stress < 0.0) 
					{
						ten_break = normal_stress / prtcl->break_Str[i];

						if (ten_break > 1.0)
						{
							if (ten_break > prtcl->mbreak)
							{
								prtcl->mbreak = ten_break;
								prtcl->neigh = i + 12;            // numbers >= 12 indicated a second neigbour
								bbreak = true;
							}
						}
					}
					else if (normal_stress >= 0.0 && model->mohr_coulomb_failure)
					{
						ten_break = shear_stress / (mc + normal_stress * tan(prtcl->angle_of_internal_friction));

						if (ten_break > 1.0)
						{
							if (ten_break > prtcl->mbreak)
							{
								prtcl->mbreak = ten_break;
								prtcl->neigh = i + 12;            // numbers >= 12 indicated a second neigbour
								bbreak = true;
							}
						}
					}
					else {
						ten_break = 0.0;
					}     
				}
			}
		}
	}
	
    return bbreak;
    
}

bool Relaxation::RelaxModel(Particle *prtcl, int threadnum)
{
    int i,j,m;

    Real dx, dy, dz, dd, uxi, uyi, uzi;
    Real fn, fy, fx, fz, xx = 0.0, yy = 0.0, zz = 0.0, anc = 0.0, alen;
    Real movement;
	
    int neig_count = 0;
    int x,y,z;

	bool flag;
	
	Real f, c, usx, usy, usz, nx, ny, nz,unx, uny, unz, ux, uy, uz;

    Particle *custom_neig_list[1500];
    Particle *neig;
    
    std::list<Particle::visc_unconnected_particle>::iterator it;
	Particle::visc_unconnected_particle tmp;

    prtcl->done_for_multithreads[threadnum] = true;
    custom_neig_list[neig_count++] = prtcl;

    if ( prtcl->is_second_neighbour_model )
    {
		
		// connected first neighbours
	    for ( i=0; i<12; i++ )
	    {
	        if ( prtcl->neighbour_list[i] )
	        {
	
	            dx = prtcl->GetXDist(prtcl->neighbour_list[i]);
	            dy = prtcl->neighbour_list[i]->pos[1] - prtcl->pos[1];
	            dz = prtcl->GetZDist(prtcl->neighbour_list[i]);
	            
	            ux = dx - prtcl->neigpos[i][0];
				uy = dy - prtcl->neigpos[i][1];
				uz = dz - prtcl->neigpos[i][2];
	            
	            dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));
	            if (dd != 0.0)
	            {
					nx = dx / dd;
					ny = dy / dd;
					nz = dz / dd;
				}
				else
				{
					nx = 0;
					ny = 0;
					nz = 0;
				}
	
				f = ux*nx + uy*ny + uz*nz;
				unx = nx * f;
				uny = ny * f;
				unz = nz * f;
				
				usx = ux - unx;
				usy = uy - uny;
				usz = uz - unz;
				
				// *** normal force
				c = (prtcl->k1 + prtcl->neighbour_list[i]->k1) / 2.0;
	            xx += unx * c;
	            yy += uny * c;
	            zz += unz * c;
	            anc += c;
	            
	            // *** shear force				
				c = (prtcl->ks1 + prtcl->neighbour_list[i]->ks1) / 2.0;
	            xx += usx * c;
	            yy += usy * c;
	            zz += usz * c;
	            anc += c;
	
	            prtcl->neighbour_list[i]->done_for_multithreads[threadnum] = true;
	            custom_neig_list[neig_count++] = prtcl->neighbour_list[i];
	        }
	    }

		// connected second neighbours
		for (i=0; i<6; i++)
		{
			if ( prtcl->second_neighbour_list[i] )
			{

	            dx = prtcl->GetXDist(prtcl->second_neighbour_list[i]);
	            dy = prtcl->second_neighbour_list[i]->pos[1] - prtcl->pos[1];
	            dz = prtcl->GetZDist(prtcl->second_neighbour_list[i]);
	            
	            ux = dx - prtcl->secneigpos[i][0];
				uy = dy - prtcl->secneigpos[i][1];
				uz = dz - prtcl->secneigpos[i][2];
	            
	            dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));
	            if (dd != 0.0)
	            {
					nx = dx / dd;
					ny = dy / dd;
					nz = dz / dd;
				}
				else
				{
					nx = 0;
					ny = 0;
					nz = 0;
				}
	
				f = ux*nx + uy*ny + uz*nz;
				unx = nx * f;
				uny = ny * f;
				unz = nz * f;
				
				usx = ux - unx;
				usy = uy - uny;
				usz = uz - unz;
				
				// *** normal force
				c = (prtcl->k2 + prtcl->second_neighbour_list[i]->k2) / 2.0;
	            xx += unx * c;
	            yy += uny * c;
	            zz += unz * c;
	            anc += c;
	            
	            // *** shear force				
				c = (prtcl->ks2 + prtcl->second_neighbour_list[i]->ks2) / 2.0;
	            xx += usx * c;
	            yy += usy * c;
	            zz += usz * c;
	            anc += c;
	            
				prtcl->second_neighbour_list[i]->done_for_multithreads[threadnum] = true;
				custom_neig_list[neig_count++] = prtcl->second_neighbour_list[i];

			}
		}
	}

	/*
	 * repulsion is considered on a 'per-particle'-basis,
	 * i.e. only if bonds are broken or the repulsion is
	 * the only mode considered in the model.
	 */

    /********************************************
	problems in rep_box were frequently related
	to an "custom_neig_list" of insuf-
	ficient size, which didn't cover all neigh-
	bours (and there are many neighbours).
	This error cost a lot of nerves and time
	already...
	Currently the size is set to 1000, but this
	might not be always sufficient.
	********************************************/
	// unconnected particles / repbox
   	if (prtcl->repulsion)
    {		
		if (prtcl->viscoelastic)
		{
			for (it=prtcl->visc_unconnected_neigh_list.begin(); it!=prtcl->visc_unconnected_neigh_list.end(); ++it)
			{
				neig = (*it).neig;
				
				if (!neig->done_for_multithreads[threadnum])
				{
					// first manage the rep-box
					neig->done_for_multithreads[threadnum] = true;
					custom_neig_list[neig_count++] = neig;
					
					dx = prtcl->GetXDist(neig);
					dy = (neig->pos[1] - prtcl->pos[1]);
					dz = prtcl->GetZDist(neig);
		
					dd = sqrt((dx*dx) + (dy*dy) + (dz*dz));
		
					alen = (*it).alen;
		
					// only replusive forces
					fn = ((prtcl->k1 + neig->k1) / 2.0) * (dd - alen);
					
					if (fn < 0.0)
					{
						if (dd != 0.0)
						{
							uxi = dx / dd;
							uyi = dy / dd;
							uzi = dz / dd;
						}
						else
						{
							uxi = 0.0;
							uyi = 0.0;
							uzi = 0.0;
						}
		
						
						fx = fn * uxi;
						fy = fn * uyi;
						fz = fn * uzi;
		
						xx += fx ;
						yy += fy ;
						zz += fz ;
						anc = anc + ((prtcl->k1 + neig->k1) / 2.0);
					}
				}
			}
		}
		
		for (i=-1; i<=1; i++)
		{
			for (j=-1; j<=1; j++)
			{
				for (m=-1; m<=1; m++)
				{
					x = prtcl->box_pos[0] + i;
					y = prtcl->box_pos[1] + j;
					z = prtcl->box_pos[2] + m;
					
					neig = NULL;
					
					if (!(y >= prtcl->repbox_dim_y || y < 0))
					{
						
						flag = true;
						
						if (x >= prtcl->repbox_dim_x || x < 0)
						{
							if (prtcl->use_periodic_boundaries_in_x)
							{
								if (x >= prtcl->repbox_dim_x)
									x -= prtcl->repbox_dim_x;
								else if (x < 0)
									x += prtcl->repbox_dim_x;
							}
							else
								flag = false;
						}
						
						if (z >= prtcl->repbox_dim_z || z < 0)
						{
							if (prtcl->use_periodic_boundaries_in_z)
							{
								if (z >= prtcl->repbox_dim_z)
									z -= prtcl->repbox_dim_z;
								else if (z < 0)
									z += prtcl->repbox_dim_z;
							}
							else
								flag = false;
						}

						if (flag)
							neig = model->rep_box[x][y][z];
					}
										
					while (neig)
					{
						if (!neig->done_for_multithreads[threadnum])
						{

							dx = prtcl->GetXDist(neig);
							dy = (neig->pos[1] - prtcl->pos[1]);
							dz = prtcl->GetZDist(neig);

							dd = sqrt(dx*dx + dy*dy + dz*dz);
							
 							alen = prtcl->GetRepAlen(neig);
							
							// only replusive forces
							if (dd < alen)
							{
								if (dd != 0.0)
								{
									uxi = dx / dd;
									uyi = dy / dd;
									uzi = dz / dd;
								}
								else
								{
									uxi = 0.0;
									uyi = 0.0;
									uzi = 0.0;
								}

								fn = ((prtcl->k1 + neig->k1) / 2.0) * (dd - alen);
								fx = fn * uxi;
								fy = fn * uyi;
								fz = fn * uzi;

								xx += fx ;
								yy += fy ;
								zz += fz ;
								anc = anc + ((prtcl->k1 + neig->k1) / 2.0);
							}

							if (prtcl->viscoelastic)
							{
								neig->repulsion = true;
								tmp.neig = neig;
								tmp.remove = false;
								tmp.alen = prtcl->GetRepAlen(neig);
								// tmp.alen = 2.0 * prtcl->radius;
	
								prtcl->visc_unconnected_neigh_list.push_back(tmp);
							}

							// manage the rep-box
							neig->done_for_multithreads[threadnum] = true;
							custom_neig_list[neig_count++] = neig;
						}
						neig = neig->next_in_box;
					}
				}
			}
		}
	}

    // unset the 'done'-flags
    for (i=0; i<neig_count; i++) custom_neig_list[i]->done_for_multithreads[threadnum] = false;
    
    if (prtcl->walls_in_x)
    {
		if (prtcl->pos[0] < prtcl->radius)
			xx -= prtcl->k1 * (prtcl->pos[0] - prtcl->radius);
		else if (prtcl->pos[0] > (prtcl->xlength - prtcl->radius))
			xx -= prtcl->k1 * (prtcl->radius - (prtcl->xlength - prtcl->pos[0]));
			
		anc += prtcl->k1;
	}
	
    if (prtcl->walls_in_z)
    {
		if (prtcl->pos[2] < prtcl->radius)
			zz -= prtcl->k1 * (prtcl->pos[2] - prtcl->radius);
		else if (prtcl->pos[2] > (prtcl->zlength - prtcl->radius))
			zz -= prtcl->k1 * (prtcl->radius - (prtcl->zlength - prtcl->pos[2]));
		
		anc += prtcl->k1;
	}
    
    // gravity formulation I
    // this formulation depends on fg as a WEIGHTFORCE
    // (cp Model::ActivateGravity())
    if (prtcl->use_gravity_force)
    {
        yy -= prtcl->fg;
        anc += prtcl->k1;
    }
    
    // the final summarizing calculation of the forces
    if ( anc != 0.0 )
    {
        anc = 1.0/anc;
        xx *= anc;
        yy *= anc;
        zz *= anc;
    }
    else
    {
        xx = 0.0;
        yy = 0.0;
        zz = 0.0;
    }

    // gravity formulation II
    // this formulation depends on fg as a DISPLACEMENT
    // (cp Model::ActivateGravity())
   	/*if (prtcl->use_gravity_force)
    {
        yy -= prtcl->fg;
    }*/

    if (prtcl->is_boundary)
    {
        if (prtcl->fix_x)
        {
            xx = 0.0;
        }
        if (prtcl->fix_y)
        {
            yy = 0.0;
        }
        if (prtcl->fix_z)
        {
            zz = 0.0;
        }
    }

    Real fover_loc = 1.0;

    if (!prtcl->dynamic_relax_thresh)
        fover_loc = prtcl->fover;
    else
    {
        movement = sqrt(xx*xx + yy*yy + zz*zz);
        fover_loc = 0.95 + (movement / prtcl->radius);
        if (fover_loc > 1.95) fover_loc = 1.95;
    }

    // the 'bottom' of the model: no y-position is allowed to be smaller than the particle-radius
    // if gravity is activated!
    if (prtcl->use_gravity_force)
    {
		if (prtcl->pos[1] + fover_loc * yy < 0.0)
			yy = (prtcl->radius - prtcl->pos[1]) / fover_loc;
	}

    xx *= fover_loc;
    yy *= fover_loc;
    zz *= fover_loc;

    while ((xx > 0.5 * prtcl->radius) || (yy > 0.5 * prtcl->radius) || (zz > 0.5 * prtcl->radius))
    {
        xx *= 0.5;
        yy *= 0.5;
        zz *= 0.5;
    }
    
    movement = sqrt(xx*xx + yy*yy + zz*zz);

	prtcl->xx = xx;
	prtcl->yy = yy;
	prtcl->zz = zz;

    if ( prtcl->relaxthreshold < movement )
    {
    
        return false;
    }
    else
    {
	    
        return true;
    }

}

inline void Relaxation::UpdateParticlePos(Particle *prtcl)
{

    prtcl->pos[0] += prtcl->xx;
    prtcl->pos[1] += prtcl->yy;
    prtcl->pos[2] += prtcl->zz;
    
    prtcl->xx = prtcl->yy = prtcl->zz = 0.0;

}

void Relaxation::Info()
{
    cout << endl;
    cout << "Threshold for relaxation: " << relaxthreshold << endl;
    cout << endl;
}

