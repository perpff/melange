#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from melange import *

workingdir = os.getcwd()

"""
create necessary objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

"""
initialize
"""
m.CreateNewModel( 50,	# int resolution_in_x = 50, 
1, 0.4, 0.3, 		# double xlength = 1.0, double ylength = 1.0, double zlength = 1.0, 
True, True, 		# bool is_next_neighbour_model = true, bool is_second_neighbour_model = true, 
True, 			# bool repulsion = true, 
True, True, 		# bool walls_in_x = false, bool walls_in_z = false, 
False, True )		# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

r.SetRelaxationThreshold(1e-9)
## good for granular materials
#r.SetOverRelaxationFactor(2.044)
## good for lattices:
r.SetOverRelaxationFactor(1.20)

e.SetStandardKandYoungsModulus(100e9);

e.SetRealKandYoungsModulus(100e9, 0.25, 0.75, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.0, 0.25, 0.0, 0.025, 0.0, 1.0)
#e.SetRealKandYoungsModulus(200e9, 0.75, 1.0, 0.0, 0.025, 0.0, 1.0)

'''
General (real) physical parameters
'''
## DEFAULT for the density is 2700 kg/m**3
e.SetDensityForAllParticles(2700)               # upper crust density
## DEFAULT for the systemsize is 100 km
e.SetScalingFactor(20000)                      # 20 km

# seconds; timestep is set to 0 bei default
e.SetTimeStep(864000000.0)	# 100.000 a

"""
Control over the boundaries,
whether they are fixed, are 
allowed to break, etc.
"""
e.EnableSideWallBreakingInZ()
#e.EnableSideWallBreakingInZAndX()
e.Unfix_z()
e.Unfix_x()

#e.RemoveSprings(-10.0, 10.0, 0.025, 10.0, -10.0, 10.0)
e.ChangeBreakingStrength( -3.5e6, -10.0, 10.0, 0.025, 0.4, -10.0, 10.0 )
e.ChangeBreakingStrength( -3.5e6, -10.0, 10.0, 0.0, 0.025, -10.0, 10.0 )

#e.Pin_Bottom_Boundary_Several_Layer(1)
e.Pin_Bottom_Boundary()
e.ActivateGravity()
## use viscoelasticity (i.e.: change the young's modulus for most particles accordingly)
e.ActivateLatticeViscoelasticity()

#e.SetNoBreakY (0.025)

# 
e.ScaleSecondNextToNextNeighbourBrStr()
# for further scaling, if 2nd-neig springs still localize to much
#e.MultiplySecNeigStrength(0.7)

### output and summary in cli
m.Info()
r.Info()

start = time.time()
r.Relax()
print("Initial relaxation took:", time.time() - start)
m.DumpVTKFile("res-"+`0`)

for t in range (1, 100001, 1):

        #d.DeformLatticeSandbox(0.0000025, 0.00625, 0.48, 0.52)
        d.DeformLattice(0.0000025)
        d.InsertDykeX(0.0000025, 0.3, 0.4, 0.6)

        start = time.time()

        r.Relax()

        print("Relaxation took:", time.time() - start)
        
        if t % 1 == 0:
	        m.DumpVTKFile("res-"+`t`)

print "end"

