#!/usr/bin/python 

"""
Copyright (C) 2012 Till Sachau <sachau@uni-mainz.de> or <till.sachau@gmail.com>

This file is part of Melange.

Melange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Melange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with Melange.  If not, see <http://www.gnu.org/licenses/>.
"""

###########

"""
General structure of the script:

	1. import melange and a number of genuine python modules + system settings. 
	Don't change them if youd don't know what you are doing.

	2. create objects from the classes provided by melange. If you don't know 
	what these terms mean: there is no reason to know anything about it right now. 
	Just copy the code below.

	3. initialze the model by telling it which resolution you want to use, which 
	boundary condition and a few other things

	4. set the material parameters and make some more finegrained settings, 
	e.g. the timestep and the relaxationthreshold

	5. tell the simulation if you want to use: 
	- viscoelasticity (default	is a purely elastic model), 
	- gravity (default is to ignore the gravity force)
	- powerlaw viscoelasticity, and
	- shear failure in addition to tensile failure

	6. the main loop consisting of deformation + relaxation + saving the result
	
Beware:

	There are many more functions availabe! You can view them in the source file
	'python_interface.cpp'. All of the functions defined there can be called from
	a Python script.

Coordinates in melange:

	just as in computer graphics, the directions/axes are usually defined by: 
	- X axis: left/right
	- Y axis: up/down
	- Z axis: front back

	origin of the coordinate system (0/0/0) is in the lower/left/back corner

Learn more about the Python language (shouldn't be needed in the beginning):

	http://docs.python.org/2/tutorial/

"""

###########

"""
1. system settings, import modules, user information
"""

import os, time
from math import *
from melange import *

workingdir = os.getcwd()

print ("")
print ("*** This simulates a rift transfer zone under extension")
print ("*** Rift segments are modeled as cracks") 
print ("*** extension is in X direction")
print ("")

print ("")
print ("*** This simulation won't develop fractures!")
print ("*** If you want to simulate fracturing,")
print ("*** review the SetNoBreak-function within this script!")
print ("")
print ("*** You might also want to disable this message in this case.")
print ("")
##################################################################

"""
2. create necessary objects
	the order is VERY important here, because the Model-class is an argument for the
	other objects
"""
m = Model();
e = ExperimentSettings(m)
d = Deformation(m)
r = Relaxation(m)

##################################################################

"""
3. initialize the model
	needs the following parameters:

	- the 1-dimensional resolution in X-direction (default is 50). 
	y/z resolutions are calculated from the x-resolution

	- the length (in model coordinates!) in x, y and z.
	
	- model type (second neighbour / first neighbour model). this should be set 
	to 'True', commonly. setting to 'False' initializes a first-enighbour model.
	
	- do we need particle repulsion? (the default 'True' is usually reasonable)
	
	- walls: an elastic wall which keeps border particles inside, 
	if they are not fixed. the default is 'False'.  
	
	- periodic boundaries: connect particles at the right/front system boundary 
	to particles at the left/back boundary? this can reduce boundary issues 
	immensely, but can also have unwanted effects. 
	therefore the default is 'False'.
"""

m.CreateNewModel( 
30, 					# int resolution_in_x = 50,
1.0, 0.5, 2.0, 			# double xlength = 1.0, double ylength = 1.0, double zlength = 1.0,
True,  					# bool is_second_next_neighbour_model = true, 
True,					# bool repulsion = true, 
False, False, 			# bool walls_in_x = false, bool walls_in_z = false, 
True, False )			# bool periodic_boundaries_in_x = false, bool periodic_boundaries_in_z = false);

##################################################################

"""
4. Finetuning
"""

# 	the relaxation threshold is identical to the tolerated possible error of 
# 	nodal displacements. It affetcs the calculation time and the precision.
#	5e-9 is quite reasonable, but thresholds as large as 1e-8 produce still
#	useful results and make the simulation MUCH faster.
r.SetRelaxationThreshold(5e-9)

#	below: set the overrelaxation factor. play around with that value
# 	reasonable values are in the limits of 0.0 < f < 2.0. 
#	try something between 1.0 and 1.9.
r.SetOverRelaxationFactor(1.0)

# 	this activates a dynamic threshold, which adjusts itself to the  
#	actual displacement of a node. 
#	default = false, because its still a bit experimental.
r.ActivateDynamicORF(False)

#	change the default Young's modulus E (uses Pascal) and Poisson ratio v. 
#
#	preset: E = 53 GPa (= granite) ; v = 0.2.
e.SetStandardYoungsModulusAndV(100e9, 0.20);

#	change the Youngs Modulus E (Pascal) / Poisson ratio v in certain domains
#	domains are cubic, given by minimum/maximum X-value, minimum/maximum Y-value
# 	and minimum/maximum Z-value
#
# 	thus: SetRealYoungAndV(E, v, x_min, x_max, y_min, y_max, z_min, z_max)
e.SetRealYoungAndV(160e9, 0.20, 0.35, 0.65, 0.4, 0.5, 0.8, 1.2)
e.SetRealYoungAndV(160e9, 0.20, 0.35, 0.65, 0.35, 0.4, 0.8, 1.2)
e.SetRealYoungAndV(160e9, 0.20, 0.0, 1.0, 0.0, 0.35, 0.0, 2.0)


# 	change default density (in kg/m**3). preset: 2750 kg/m**3 (upper crust)
e.SetDensityForAllParticles(2700)
          
# 	systemsize in m. its a scale factor, which is multipcated by the model dimensions.
#	if the model size is 1.0, 0.5, 2.0 and the scale is 200000, than the physical
#	dimension is 200 km X 100 km X 400 km
#
#	preset: 100000 m
e.SetScalingFactor(200000)

#	change the Viscosity (in Pa s) in certain domains
#	domains are cubic, given by minimum/maximum X-value, minimum/maximum Y-value
# 	and minimum/maximum Z-value
#
# 	thus: SetRealYoungAndV(viscosity, x_min, x_max, y_min, y_max, z_min, z_max)
e.SetViscosity( 1e25, 0.0, 1.0, 0.45, 0.5, 0.0, 2.0 )
e.SetViscosity( 1e22, 0.0, 1.0, 0.0, 0.35, 0.0, 2.0 )
e.SetViscosity( 1e22, 0.35, 0.65, 0.35, 0.4, 0.8, 1.2 )

# 	set the timestep associated with a deformation increment
#	given in in seconds, and preset is just 0
e.SetTimeStep(125000 * 365 * 24 * 60 * 60)	# 125000 a

#	by default, springs attached to boundary particles can not break
# 	you can change that with the functions below:
#e.EnableSideWallBreakingInZ()
#e.EnableSideWallBreakingInZAndX()

#	by default, boundary particles can not move in the main axial direction
#	e.g.: the left and right boundary particles (which limit the system on the 
#	X-axis) can move only in Y and Z direction.
#
# 	you can unfix these particles with the commands below. This might be useful
# 	if you use periodic boundaries or if you want the system to bulge under
# 	uniaxial compression
#e.Unfix_z()
#e.Unfix_x()
#e.Unfix_y()

# 	if you prefer parts of your model as a granular material, you can remove
#	the springs from the particles. Don't forget to activate repulsion in
# 	the initialization function under point 2!
#	
#	Remove sprngs in cubical domains:
#	e.RemoveSprings(x_min, x_max, y_min, y_max, z_min, z_max)
#e.RemoveSprings(0.0, 1.0, 0.025, 1.0, 0.0, 1.0)

#	change the tensile breaking strength for within cubic domains. 
#	Standard breaking strength is 1 MPa - which is low, but still reasonable for 
#	geomaterials. if you later activate Mohr-Coulomb shear breaking, the shear 
#	strength will be calculated from the tensile strength and the angle of 
#	internal friction.
#
#	as usualy, cubic domains are given by minimum/maximum X-value, 
#	minimum/maximum Y-value and minimum/maximum Z-value
# 	the breaking strength is given in Pascal
# 	e.ChangeBreakingStrength( breaking_strength, x_min, x_max, y_min, y_max, z_min, z_max )
e.ChangeBreakingStrength( -1e6, 0.0, 1.0, 0.0, 1.0, 0.0, 2.0 )

#	Set a gaussian distribution on the breaking strength. Set for a cubical domain.
# 	Standard is NO distribution.
#
#	e.SetGaussianStrengthDistribution(mean, standard_deviation, x_min, x_max, y_min, y_max, z_min, z_max)
e.SetGaussianStrengthDistribution(1.0, 0.01, 0, 1.0, 0, 1.0, 0, 2.0)

#	This function inserts artificial cracks into the material. Here, these cracks
#	will resemble the geometry of the rift segments at a rift transfer zone.
#
#	The definition of the planes is not completely trivial.
# 	the first nine parameters are the x/y/z cordinates of 3 points on an
#	arbitrary plane.
#	the next 6 paramters define the cubical domain, where this plane shall insert
# 	the crack.
#	so:
#	e.SeverSpringsCrossingPlane	( x_1, y_1, z_1, x_2, y_2, z_2, x_3, y_3, z_3, 
#				 x_min, x_max, y_min, y_max, z_min, z_max)
e.SeverSpringsCrossingPlane	( 0.35, 1.0, 0.0, 0.35, 2.0, 1.0, 0.35, 0.0, 2.0, 
				0.35, 0.65, 0.4, 1.0, 0.0, 1.1 )
e.SeverSpringsCrossingPlane	( 0.65, 1.0, 0.0, 0.65, 1.0, 2.0, 0.65, 0.0, 2.0, 
				0.35, 0.65, 0.4, 1.0, 0.9, 2.0 )
e.SeverSpringsCrossingPlane	( 0.35, 1.0, 1.1, 0.35, 0.0, 1.1, 0.65, 1.0, 1.25, 
				0.34, 0.66, 0.4, 1.0, 1.1, 1.25 )
e.SeverSpringsCrossingPlane	( 0.35, 1.0, 0.75, 0.35, 0.0, 0.75, 0.65, 1.0, 0.9, 
				0.34, 0.66, 0.4, 1.0, 0.75, 0.9 )
				
##################################################################

"""
5. tell the simulation if you want to use: 
	- viscoelasticity (default	is a purely elastic model), 
	- gravity (default is to ignore the gravity force)
	- powerlaw viscoelasticity, and
	- shear failure in addition to tensile failure
"""

#	activate gravity. The bottom boundary shoud befixed for this - otherwise
#	particles just 'fall down'
e.Pin_Bottom_Boundary()
e.ActivateGravity()

# 	activate viscoelasticity in the simulation
e.ActivateLatticeViscoelasticity()

# 	consider the stress vector - derived from the complete nodal stress tensor -
#	instead of the spring force/stress to determine whether a spring will break?
# 	default is: don't! the function is stable, but the scheme is experimental.
e.SetTensorFailureMode(False)

#	if you want to use power law viscosity instead of Newtonian viscosity in 
#	certain domains, you can set the temperature and the Power law parametes
#	with the functions below.
# 	as usual: done for particles within cubic domains.
#
# 	Tenperature:
#	e.SetTemperature( float minx , float maxx , float miny , float maxy , float minz , float maxz, float temperature);
# 	Power law (A is the prefactor, Ea the activation energy, Va the activation volume, n the exponent)
# 	e.SetPowerLawViscosity(float minx , float maxx , float miny , float maxy , float minz , float maxz, float A, float Ea, float Va, float n)

#	Use Mohr-Coulomb failure, i.e. activate shear breaking of springs
#	default is 'True',
#	default angle of internal friction = pi / 6.0;
e.Activate_Mohr_Coulomb_Failure(True)

#	if you want to change the angle of internal friction within a cubic domain,
# 	you can do it the usual way:
#	e.Set_Angle_Of_Internal_Friction(angle, _min, x_max, y_min, y_max, z_min, z_max)
e.Set_Angle_Of_Internal_Friction(pi/3.73, 0.0, 1.0, 0.0, 0.3, 0.0, 1.0)
e.Set_Angle_Of_Internal_Friction(pi/4.11, 0.0, 1.0, 0.3, 0.4, 0.0, 1.0)
e.Set_Angle_Of_Internal_Friction(pi/5.2, 0.0, 1.0, 0.4, 0.5, 0.0, 1.0)

#	define a layer between the model-bottom and the given Y-value, where breaking
#	of spring is prohibited. Can be used to prohibit fracturing throughouth the
# 	whole system (as done below)
#	This MUST be called AFTER the gravity activation!!!
#
#	e.SetNoBreakY (Y_max)
e.SetNoBreakY(1.0)

##################################################################

"""
6. Start the actual simulation

Careful:
	this simulation won't fracture! we set the setnobreak-function
	above to 1.0! If you want to model something involving fracureing: deactivate
	this function or set it to 0.1 or something similar!
"""

# 	print a summary of our settings
m.Info()
r.Info()

# 	make a basic relaxation and save a result-file
#	we measure the time, using the time-module provided by python
start = time.time()
#	this calls the basic relaxation function
r.Relax()
#	this prints the duration of the current relaxation
print("Initial relaxation took:", time.time() - start)
# 	and THIS saves the system state in the file "res-0.vtu"
# 	you can view the file with Paraview
m.DumpVTKFile("res-0")


#	here we start a loop, consisting of deformation - relaxation - save of result
for t in range (1, 270, 1):

	#	deform the lattice. Here: uniaxial extension along X-axis.
	#	extensino is given in model coordinates, so if the old right system 
	#	boundary was initially located at 1.2, its located at 1.20125 after 
	#	the deformation below.
	d.DeformLattice(0.00125)

	#	save the current time
	start = time.time()

	#	relax the system
	r.Relax()
	
	#	print out the computation time
	print("Timestep:", t, ". Relaxation took:", time.time() - start)

	#	save result as "res-t.vtu", where t is the number of the timestep
	m.DumpVTKFile(f"res-{t}")

print ("end")

